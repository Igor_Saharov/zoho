<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use Illuminate\Routing\Router;

Route::get('/', function () {
    return view('welcome');
});

Route::group([
    'prefix'=>'user'
],function(Router $router){
    $router->get('allusers','UserController@allUsers');
    $router->get('sing-in','UserController@index');
    $router->get('get-user','UserController@getUser');
    $router->post('profile','UserController@login');
    });

Route::group([
    'prefix'=>'url',
],function (Router $router){
    $router->get('get','UrlController@get');
    $router->post('','UrlController@index');
    $router->put('','UrlController@update');
    $router->delete('','UrlController@delete');
});

Route::get('/settings', 'SettingsController@index')->name('settings');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
