
/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

window.Vue = require('vue');
window.Axios = require('axios');
window.bootstrap = require('bootstrap');
window.VuePassport = require('vue-passport');
/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */
Vue.component('user-function-page',
    require('./components/user_page/FromUrlCrud'));


Vue.component('sing-in-component',
    require('./components/SingInComponent.vue'));

Vue.component('profile-component',
    require('./components/ProfileComponent.vue'));

Vue.component(
    'passport-authorized-clients',
    require('./components/passport/AuthorizedClients.vue')
);

Vue.component(
    'passport-clients',
    require('./components/passport/Clients.vue')
);

Vue.component(
    'passport-personal-access-tokens',
    require('./components/passport/PersonalAccessTokens.vue')
);

var instance = axios.create();
const app = new Vue({
    el: '#app',
    methods: {
        handleLogin (payload) {
            this.$store.dispatch('setUser', payload.authUser);
            instance.defaults.headers.common['Authorization'] = payload.headers.Authorization;
        },
        handleErrors (errors) {
            alert('Authorization error' + errors)
        }
    }
});
