<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Url extends Model
{
    protected $fillable=[
        'id','origin_url','cut_url','user_id','created_at','updated_at'
    ];
    protected $table = 'urls';
}
