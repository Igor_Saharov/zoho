<?php

namespace App\Http\Controllers;

use App\Http\Requests\Url\Create;
use App\Service\UrlService;
use Illuminate\Http\Request;

class UrlController extends Controller
{
    protected $createColumns = [
        'origin_url','user_id'
    ];

    public function index(Create $request , UrlService $urlService )
    {
        $data = $request->only($this->createColumns);
        $data['cut_url'] = $urlService->generateHash();

        $result = $urlService->create($data);
        if ($result){
            return $result;
        }
        else{
            return response()->json([
                'massage'=>'Новую записть создать не удалось'
            ],500);
        }

    }

    public function get(Request $request, UrlService $urlService )
    {
        $data = $request->all();
       return $urlService->getUsersUrls($data);
    }

    public function update(Request $request , UrlService $urlService)
    {
        return $urlService->update($request->all());
    }

    public function delete(Request $request , UrlService $urlService)
    {
        if ($urlService->delete($request->only(['id'])) == 1){
            return response()->json([
                'massage'=>'Запись была удалена'
            ]);
        }
        else{
            return response()->json([
                'massage'=>'Запись не была удалена'
            ]);
        }
    }
}
