<?php

namespace App\Http\Controllers;

use App\Http\Requests\LoginUserRequest;
use App\User;
use Illuminate\Http\Request;
use App\Service\UserService;

class UserController extends Controller
{
    public function index()
    {
        return view('User/sing_in');
    }

    public function getUser(UserService $service)
    {
        return $service->getUser();

    }

    public function login(LoginUserRequest $request , UserService $service)
    {
        $data = $request->all();
        $createStatus = $service->login($data);
        if ($createStatus['status'] === true){
            return view('User/profile',$data);

        }
    }

    public function allUsers(UserService $service)
    {
        return $service->getAllUsers();
    }
}
