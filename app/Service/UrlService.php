<?php
/**
 * Created by PhpStorm.
 * User: igor
 * Date: 20.05.18
 * Time: 22:04
 */

namespace App\Service;


use App\Url;

class UrlService
{
    public function generateHash()
    {
        $value = str_random(random_int(10,15));
        if ($this->checkExistRow($value) === true){
            $this->generateHash();
        }
        else{
            return $value;
        }
    }

    public function checkExistRow(string $value)
    {
        return Url::query()->where('cut_url',$value)->exists();
    }

    public function create(array  $data)
    {
        return Url::query()->create($data);
    }

    public function getUsersUrls(array $data)
    {
        return Url::query()->where($data)->get();
    }

    public function update(array $data)
    {
        return Url::query()->where('id','=',$data['id'])->update($data);
    }

    public function delete($id)
    {
        return Url::query()->where('id',$id['id'])->delete();
    }
}