<?php
/**
 * Created by PhpStorm.
 * User: oleg
 * Date: 4/18/18
 * Time: 5:17 PM
 */

namespace App\Service;

use App\User;
use Illuminate\Support\Facades\Auth;

class UserService
{

    public function getAllUsers()
    {
        return User::query()->get();
    }

    public function login($data)
    {
        $resultCreate = User::query()->create($data);
        if ($resultCreate != null){
            return [
                'status'=>true,
                'data'=>$data
            ];
        }

    }


    public function getUser()
    {
        return Auth::user();
    }
}