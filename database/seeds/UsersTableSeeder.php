<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        for ($i=1;$i<5;$i++){
            DB::table('users')->insert([
                'name'=>str_random(12),
                'email'=>str_random(15),
                'password' => '$2y$10$FBQRUlUS2UJc65XTNU7nselfk7KkzkMaRDTGlyoVHmqaEzJqi2YUK'//123
            ]);
             }
    }
}
